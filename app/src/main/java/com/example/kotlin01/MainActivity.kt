package com.example.kotlin01

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var lblSaludo: TextView
    private lateinit var txtSaludo: EditText
    private lateinit var btnSaludar: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnCerrar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        lblSaludo = findViewById(R.id.lblSaludo)
        txtSaludo = findViewById(R.id.txtSaludo)
        btnSaludar = findViewById(R.id.btnSaludar)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnCerrar = findViewById(R.id.btnCerrar)

        btnSaludar.setOnClickListener {
            val nombre = txtSaludo.text.toString()

            if(nombre == "") {
                Toast.makeText(
                    this@MainActivity,
                    "Favor de ingresar el nombre",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else {
                lblSaludo.text = "Hola, $nombre"
            }
        }

        btnLimpiar.setOnClickListener {
            txtSaludo.text.clear()
            lblSaludo.text = ""
        }

        btnCerrar.setOnClickListener {
            finish()
        }
    }
}